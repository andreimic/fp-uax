module Lectures.Six where

max3 :: Int -> Int -> Int -> Int
max3 a b c =
  max3H (max3H a b) c
  {-((\ x y -> if x > y then x else y) a b)-}

-- Helper for max3
max3H :: Int -> Int -> Int
max3H a b =
  if a > b
    then a
    else b
{-
 -max :: Int -> Int -> Int
 -max a b =
 -  if a > b then a else b
 -}

max3' :: Int -> Int -> Int -> Int
max3' a b c =
  | a >= b && a >= c = a
  | b >= c && b >= a = b
  | Otherwise = c
