module Uaex3.Two where

max3 :: Int -> Int -> Int -> Int
max3 x y z =
  max (max x y) (max y z)
