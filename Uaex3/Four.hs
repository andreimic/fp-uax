module Uaex3.Four where

data FourTree = Leaf Char | Node String [FourTree]