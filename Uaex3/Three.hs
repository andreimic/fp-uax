module Uaex3.Three where

import Data.List (intersect)

vowelCount :: String -> Int
vowelCount str =
  length (str `intersect` "aeiou")