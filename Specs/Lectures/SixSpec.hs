module Specs.Lectures.SixSpec where

import Test.Hspec
import Lectures.Six

main = hspec $ do
  describe "max3" $ do
    it "returns 3 when given 1 2 3" $
      max3' 1 2 3 `shouldBe` 3
