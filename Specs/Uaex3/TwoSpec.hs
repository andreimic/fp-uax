module Specs.Uaex3.TwoSpec where

import Test.Hspec
import Uaex3.Two

main = hspec $ do
  describe "max3" $ do
    it "returns 3 when given 1, 2, 3" $
      max3 1 2 3 `shouldBe` 3

    it "returns 1 when given 1, 1, 1" $
      max3 1 1 1 `shouldBe` 1

