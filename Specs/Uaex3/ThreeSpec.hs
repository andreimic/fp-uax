module Specs.Uaex3.ThreeSpec where

import Test.Hspec
import Uaex3.Three

main = hspec $ do
  describe "vowelCount" $ do
    it "returns 0 when given 'bcdfghjkl'" $
      vowelCount "bcdfghjkl" `shouldBe` 0

    it "returns 1 when given 'abcdfghjkl'" $
      vowelCount "abcdfghjkl" `shouldBe` 1
