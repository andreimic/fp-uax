module Specs.Uaex2.OneSpec where

import Test.Hspec
import Uaex2.One

main = hspec $ do
  describe "nFactFold" $ do
    it "returns 1 when given 0" $
      nFactFold 0 `shouldBe` 1

    it "returns 1 when given 1" $
      nFactFold 1 `shouldBe` 1

  describe "nFactRec" $ do
    it "returns 1 when given 0" $
      nFactRec 0 `shouldBe` 1

    it "returns 1 when given 1" $
      nFactRec 1 `shouldBe` 1

  describe "nFactPrel" $ do
    it "returns 1 when given 0" $
      nFactPrel 0 `shouldBe` 1

    it "returns 1 when given 1" $
      nFactPrel 1 `shouldBe` 1
