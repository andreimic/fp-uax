module Specs.Uaex2.ThreeSpec where

import Test.Hspec
import Uaex2.Three

main = hspec $ do
  describe "revlist" $ do
    it "returns [] when given []" $
      revlist [] `shouldBe` []

    it "returns [1,2,3] when given [3,2,1]" $
      revlist [3,2,1] `shouldBe` [1,2,3]
