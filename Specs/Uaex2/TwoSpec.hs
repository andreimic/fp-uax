module Specs.Uaex2.TwoSpec where

import Test.Hspec
import Uaex2.Two

main = hspec $ do
  describe "fun" $ do
    it "returns [] when given [] []" $
      fun [] [] `shouldBe` []

    it "returns L1 when given L1 []" $
      fun [1,2] [] `shouldBe` [1,2]

    it "returns [1,2,3] when given [] [1,1,2,2,3,3]" $
      fun [] [1,1,2,2,3,3] `shouldBe` [1,2,3]
