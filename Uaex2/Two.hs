module Uaex2.Two where

fun :: [Int] -> [Int] -> [Int]
fun uxs [] = uxs
fun uxs xs =
  fun (uxs ++ (if ((head xs) `notElem` uxs) then [head xs] else [])) (tail xs)
