module Uaex2.One where

--n! with fold
nFactFold :: Int -> Int
nFactFold 0 = 1
nFactFold x = 
  foldl (*) x (take (x-1) [1..])

--n! with recursion
nFactRec :: Int -> Int
nFactRec 0 = 1
nFactRec x =
  x * nFactRec (x-1)

--n! with Prelude function
nFactPrel :: Int -> Int
nFactPrel x =
  product [1..x]
